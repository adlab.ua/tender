import {MODULE_NAME} from './constants'
import {requireAll} from './../common/requireAll'

let application = angular.module(MODULE_NAME, ['ui.router', 'ngResource']);

application.config(['$stateProvider', ($stateProvider) => {
    $stateProvider
        .state('profile', {
            url: '/user/me',
            controller: 'ProfileCtrl',
            template: require('./views/profile.html')
        })
}]);

requireAll(require.context("./", true, /.jsx?$/));
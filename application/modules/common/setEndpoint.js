import url from 'url'
import config from './../../config/index'

export const setEndpoint = (uri) => url.resolve(config.api.endpoint, uri);
import {MODULE_NAME} from './constants'
import {requireAll} from './../common/requireAll'

let application = angular.module(MODULE_NAME, ['ui.router', 'ngResource']);

application.config(['$stateProvider', ($stateProvider) => {
    $stateProvider
        .state('asd', {
            url: '/ads/:id',
            controller: 'AdvertisementCtrl',
            template: require('./views/advertisement.html')
        })
}]);

requireAll(require.context("./", true, /.jsx?$/));
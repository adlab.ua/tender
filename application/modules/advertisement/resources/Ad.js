import {MODULE_NAME} from './../constants'
import {setEndpoint} from './../../common/setEndpoint'

angular.module(MODULE_NAME).factory("Ad", ['$resource', $resource => $resource(setEndpoint("/advert/:id"))]);
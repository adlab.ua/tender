var path = require('path');
var webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {

    entry: path.resolve(__dirname, './application/index.js'),
    output: {
        path: path.resolve(__dirname, './public/js'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            { test: /\.jsx?$/, exclude: [/node_modules|bower_components/], include: path.resolve('./application'), loader: 'babel-loader' },
            { test: /\.css$/, loader: 'style!css' },
            { test: /\.html$/, loader: 'raw-loader' }
        ]
    },
    externals: {
        "angular": "angular"
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'application/index.html',
            filename: 'index.html'
        })
    ]
};
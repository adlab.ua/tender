import application from './application'
import local from './application.local'

export default Object.assign({}, application, local)
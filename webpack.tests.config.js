const path = require('path')

const config = {
    entry: path.resolve('./test/index.js'),
    output: {
        filename: 'bundle.test.js',
        path: path.resolve('./public/js'),
        publicPath: '/'
    },
    module: {
        loaders: [
            { test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader' },
            { test: /\.css$/, loader: 'style!css' },
            { test: /\.json$/, loader: 'json-loader' }
        ]
    },
    externals: {
        'jsdom': 'window',
        'react/lib/ExecutionEnvironment': true,
        'react/lib/ReactContext': true
    },
    devtool: 'cheap-module-eval-source-map'
}

module.exports = config;
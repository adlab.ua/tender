import './modules/application/index'
import './modules/advertisement/index'
import './modules/user/index'

let tender = angular.module('tender', [
    'ui.router', 'ngResource',
    
    'tender.application',
    'tender.ads',
    'tender.user'
]);

tender.config(['$urlRouterProvider', ($urlRouterProvider) => {
    $urlRouterProvider.otherwise("/");
}]);

tender.run(() => {

});
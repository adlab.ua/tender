import {MODULE_NAME} from './../constants'
import {SignInFilter} from './../filters/SignInFilter'

angular.module(MODULE_NAME).controller("SignInCtrl", ['$scope', 'Auth', ($scope, Auth) => {
    $scope.onSubmit = e => {
        $scope.error = false;
        e.preventDefault();

        SignInFilter.setData($scope.form).isValid().then(() => Auth.create($scope.form)).catch(errors => {
            $scope.error = true
        })
    }
}]);
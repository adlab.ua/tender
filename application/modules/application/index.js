import {MODULE_NAME} from './constants'
import {requireAll} from './../common/requireAll'

let application = angular.module(MODULE_NAME, ['ui.router']);

application.config(['$stateProvider', ($stateProvider) => {
    $stateProvider
        .state('application', {
            url: '/',
            controller: 'ApplicationCtrl',
            template: require('./views/application.html')
        })
}]);

requireAll(require.context("./", true, /.jsx?$/));
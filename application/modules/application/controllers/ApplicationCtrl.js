import {MODULE_NAME} from './../constants'

angular.module(MODULE_NAME).controller("ApplicationCtrl", ['$scope', ($scope) => {
    $scope.name = "Igor";

    $scope.test = () => {
        alert("Test")
    }
}]);
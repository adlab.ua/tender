import {MODULE_NAME} from './../constants'
import {setEndpoint} from './../../common/setEndpoint'

angular.module(MODULE_NAME).factory("Profile", ['$resource', $resource => $resource(setEndpoint("/user/:id"))]);
angular.module(MODULE_NAME).factory("Auth", ['$resource', $resource => $resource(setEndpoint("/auth"))]);
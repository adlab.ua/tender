import {InputFilter, Callback} from 'input-filter'

export const SignUpFilter = InputFilter.factory({
   email: {
       required: true,
       validators: [
           new Callback(value => /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value))
       ]
   },
   username: {
       required: true
   },
   password: {
       required: true
   },
   confirmation: {
       required: true,
       validators: [
           new Callback((value, {password}) => value == password)
       ]
   }
});
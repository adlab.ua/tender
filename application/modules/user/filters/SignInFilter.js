import {InputFilter, Callback} from 'input-filter'

export const SignInFilter = InputFilter.factory({
   email: {
       required: true,
       validators: [
           new Callback(value => /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value))
       ]
   },
   password: {
       required: true
   }
});